#!/bin/bash

set -e

function check() {
  ruby app.rb "$2"  | grep "$3" >/dev/null && echo "$1:ok" || echo "$1:error"            
}   

echo 'Running acceptance tests for gchecker'
check '01' 'spec/samples/Gemfile.valid' 'ok'
check '02' 'spec/samples/Gemfile.invalid.empty' 'error'
#check '03' 'spec/samples/Gemfile.invalid.source' 'error'
#check '04' 'spec/samples/Gemfile.invalid.ruby' 'error'
